package be.ap.weather.station;

import be.ap.weather.station.services.DirectoryScanner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {
	@Autowired
	private DirectoryScanner directoryScanner;

	@Test
	public void contextLoads() {
		directoryScanner.findFilesBetween("100020", LocalDate.of(2019,1,29), LocalDate.of(2019,1,29));
	}

}

