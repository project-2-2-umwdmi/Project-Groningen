package be.ap.weather.station.controller;

import be.ap.weather.station.models.TimeStampedData;
import be.ap.weather.station.services.DirectoryScanner;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@RestController
public class WeatherController {
    @Autowired
    private DirectoryScanner directoryScanner;

    @RequestMapping(value = {"/weather/{stationnumber}"}, method = RequestMethod.GET)
    public JsonNode getWeatherData(@PathVariable("stationnumber") String stationNumber,
                                                         @RequestParam(value = "start") String start,
                                                         @RequestParam(value = "end") String end) {
        DateTimeFormatter dtf =  DateTimeFormatter.ofPattern("yyyy-MM-dd");
        File[] files =  directoryScanner.findFilesBetween(stationNumber, LocalDate.parse(start, dtf), LocalDate.parse(end, dtf));

        ArrayNode arr = JsonNodeFactory.instance.arrayNode();

        for (File f: files) {
            JsonNode json = null;
            try {
                 json = new ObjectMapper().readTree(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
            arr.add(json);
        }
        JsonNode weatherDataJSON = arr;

        return weatherDataJSON;
    }
}
