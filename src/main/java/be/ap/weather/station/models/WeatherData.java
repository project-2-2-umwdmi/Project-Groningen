package be.ap.weather.station.models;

import java.util.Objects;

public class WeatherData {
    private String
            temp,
            dewp,
            stp,
            slp,
            visib,
            wdsp,
            prcp,
            sndp,
            frshtt,
            cldc,
            wnddir;

    public WeatherData() {
    }

    public WeatherData(String temp, String dewp, String stp, String slp, String visib, String wdsp, String prcp, String sndp, String frshtt, String cldc, String wnddir) {
        this.temp = temp;
        this.dewp = dewp;
        this.stp = stp;
        this.slp = slp;
        this.visib = visib;
        this.wdsp = wdsp;
        this.prcp = prcp;
        this.sndp = sndp;
        this.frshtt = frshtt;
        this.cldc = cldc;
        this.wnddir = wnddir;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getDewp() {
        return dewp;
    }

    public void setDewp(String dewp) {
        this.dewp = dewp;
    }

    public String getStp() {
        return stp;
    }

    public void setStp(String stp) {
        this.stp = stp;
    }

    public String getSlp() {
        return slp;
    }

    public void setSlp(String slp) {
        this.slp = slp;
    }

    public String getVisib() {
        return visib;
    }

    public void setVisib(String visib) {
        this.visib = visib;
    }

    public String getWdsp() {
        return wdsp;
    }

    public void setWdsp(String wdsp) {
        this.wdsp = wdsp;
    }

    public String getPrcp() {
        return prcp;
    }

    public void setPrcp(String prcp) {
        this.prcp = prcp;
    }

    public String getSndp() {
        return sndp;
    }

    public void setSndp(String sndp) {
        this.sndp = sndp;
    }

    public String getFrshtt() {
        return frshtt;
    }

    public void setFrshtt(String frshtt) {
        this.frshtt = frshtt;
    }

    public String getCldc() {
        return cldc;
    }

    public void setCldc(String cldc) {
        this.cldc = cldc;
    }

    public String getWnddir() {
        return wnddir;
    }

    public void setWnddir(String wnddir) {
        this.wnddir = wnddir;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherData that = (WeatherData) o;
        return Objects.equals(temp, that.temp) &&
                Objects.equals(dewp, that.dewp) &&
                Objects.equals(stp, that.stp) &&
                Objects.equals(slp, that.slp) &&
                Objects.equals(visib, that.visib) &&
                Objects.equals(wdsp, that.wdsp) &&
                Objects.equals(prcp, that.prcp) &&
                Objects.equals(sndp, that.sndp) &&
                Objects.equals(frshtt, that.frshtt) &&
                Objects.equals(cldc, that.cldc) &&
                Objects.equals(wnddir, that.wnddir);
    }

    @Override
    public int hashCode() {
        return Objects.hash(temp, dewp, stp, slp, visib, wdsp, prcp, sndp, frshtt, cldc, wnddir);
    }
}
