package be.ap.weather.station.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class TimeStampedData {
    private LocalDateTime dateTime;
    private WeatherData weatherData;

    public TimeStampedData() {
    }

    public TimeStampedData(LocalDateTime dateTime, WeatherData weatherData) {
        this.dateTime = dateTime;
        this.weatherData = weatherData;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public WeatherData getWeatherData() {
        return weatherData;
    }

    public void setWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeStampedData that = (TimeStampedData) o;
        return Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(weatherData, that.weatherData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, weatherData);
    }
}
