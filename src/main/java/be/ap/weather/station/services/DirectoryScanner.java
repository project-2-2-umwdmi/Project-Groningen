package be.ap.weather.station.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;

@Service
@Validated
public class DirectoryScanner {
    @Value("${data.path}")
    @NotNull
    private String path;

    public File[] findFilesBetween(String stationNumber, LocalDate startDate, LocalDate endDate) {
        File directory = new File(path);

        if (directory.isFile()) {
            throw new IllegalStateException("The path configuration is not correctly configured.");
        }

        final File[] stationDirectory = Arrays.stream(Objects.requireNonNull(directory.listFiles(File::isDirectory)))
                .filter(file -> file.getName().equals(stationNumber))
                .toArray(File[]::new);

        if (stationDirectory.length < 1) {
            throw new IllegalArgumentException("Station not found.");
        } else if (stationDirectory.length > 1) {
            throw new IllegalStateException("An exact name search returned more than 1 result.");
        }

        final Set<File> requiresExtraFilteringOnMonth = new HashSet<>();
        final File[] yearDirectories = stationDirectory[0].listFiles((dir, name) -> {
            try {
                final int year = Integer.parseInt(name);
                if (year == startDate.getYear()) {
                    requiresExtraFilteringOnMonth.add(new File(dir.getPath(), name));
                } else if (year == endDate.getYear()) {
                    requiresExtraFilteringOnMonth.add(new File(dir.getPath(), name));
                }

                return year > startDate.getYear() && year < endDate.getYear();
            } catch (NumberFormatException e) {
                return false;
            }
        });

        final Set<File> jsonFiles = new HashSet<>();
        //Collect everything in these maps they don't need further filtering
        if (yearDirectories != null) {
            Arrays.stream(yearDirectories).forEach(
                    yearDirectory -> Arrays.stream(Objects.requireNonNull(yearDirectory.listFiles(File::isDirectory))).forEach(
                            addFilesFromMonthDirectory(jsonFiles)
                    )
            );
        }

        final Set<File> requiresExtraFilteringOnDay = new HashSet<>();
        final Set<File> monthDirectories = new HashSet<>();
        requiresExtraFilteringOnMonth.forEach(yearDirectory ->
                monthDirectories.addAll(Arrays.asList(Objects.requireNonNull(yearDirectory.listFiles((dir, name) -> {
                            try {
                                final int month = Integer.parseInt(name);
                                if (month == startDate.getMonthValue()) {
                                    requiresExtraFilteringOnDay.add(new File(dir.getPath(), name));
                                } else if (month == endDate.getMonthValue()) {
                                    requiresExtraFilteringOnDay.add(new File(dir.getPath(), name));
                                }

                                return month > startDate.getMonthValue() && month < endDate.getMonthValue();
                            } catch (NumberFormatException e) {
                                return false;
                            }
                        })))
                ));

        monthDirectories.forEach(addFilesFromMonthDirectory(jsonFiles));

        requiresExtraFilteringOnDay.forEach(monthDirectory -> Arrays.stream(Objects.requireNonNull(monthDirectory.listFiles((dir, name) -> {
            try {
                final int day = Integer.parseInt(name);
                return day >= startDate.getDayOfMonth() && day <= endDate.getDayOfMonth();
            } catch (NumberFormatException e) {
                return false;
            }
        }))).forEach(addFilesFromDayDirectory(jsonFiles)));
        return jsonFiles.toArray(File[]::new);
    }

    private Consumer<File> addFilesFromMonthDirectory(Set<File> jsonFiles) {
        return monthDirectory -> Arrays.stream(Objects.requireNonNull(monthDirectory.listFiles(File::isDirectory))).forEach(
                addFilesFromDayDirectory(jsonFiles)
        );
    }

    private Consumer<File> addFilesFromDayDirectory(Set<File> jsonFiles) {
        return dayDirectory -> jsonFiles.addAll(Arrays.asList(Objects.requireNonNull(dayDirectory.listFiles((dir, name) -> name.toLowerCase().endsWith(".json")))));
    }
}
